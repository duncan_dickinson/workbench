package name.dickinson.duncan.csv

import groovy.sql.Sql
/**
 * 
 * HSQL restricts the location of CSV (text) tables by default.
 * You will most likely need to call the following in your code:
 *   System.setProperty("textdb.allow_full_path", "true")
 * See: http://hsqldb.org/doc/guide/texttables-chapt.html#ttc_issues
 * 
 * @author dickinsd
 *
 */
class DB {

	private static Sql cnn = Sql.newInstance("jdbc:hsqldb:mem:mymemdb",
			"SA",
			"",
			"org.hsqldb.jdbc.JDBCDriver")

	private static String DEFAULT_COL_TYPE = 'varchar(2K)'

	private DB(){}

	public static void createTextTable(tableName, colList, fileName, separator = ',') {
		def colDef = ""
		for (col in colList) {
			colDef += "$col $DEFAULT_COL_TYPE,"
		}
		colDef = colDef[0..-2]

		// Hint - the DESC keyword makes the table read-only (go figure)
		def sql = """
			DROP TABLE IF EXISTS ${tableName};
			CREATE TEXT TABLE ${tableName} (${colDef});
			SET TABLE ${tableName} SOURCE '${fileName};ignore_first=true;fs=${separator}' DESC
			"""

		cnn.execute sql.toString()
	}

	public static void createTempTable(tableName, selectStatement) {
		def sql= """
		DROP TABLE IF EXISTS session.${tableName};
		DECLARE LOCAL TEMPORARY TABLE ${tableName} AS (
			${selectStatement}) 
		WITH DATA
		"""
		cnn.execute sql
	}
	
	public static java.sql.ResultSet executeQuery (String qry) {
		return cnn.execute(qry)
	}
	
	public static void close(){
		cnn.close();
	}
}
