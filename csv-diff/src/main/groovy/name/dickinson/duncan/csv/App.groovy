package name.dickinson.duncan.csv

import java.nio.file.Files
import java.nio.file.Paths



/*******************************************************************************
 * Copyright (c) 2014, Duncan Dickinson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * 	  this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * 	  this list of conditions and the following disclaimer in the
 * 	  documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

class App {
	static public String VERSION = '0.1.0'

	static boolean check_file_param(file) {
		if(Files.notExists(Paths.get(file))){
			return false
		}
		return true
	}

	static boolean check_dir_param(dir) {
		if(Files.isDirectory(Paths.get(dir))){
			return true
		}
		return false
	}

	static void cli_err(text) {
		System.err << text + "\n"
		System.exit(1)
	}

	static void main(String[] args) {
		CliBuilder cli = new CliBuilder(usage:'csvdiff [options]')
		cli.with{
			h longOpt: 'help', 					'Show usage information'
			v longOpt: 'version',				'Show the current version of the application'
			i longOpt: 'idColumn',	args:1, 	'The ID column (defaults to ID)'
			o longOpt: 'old', 		args:1,		'The CSV file containing the old data.'
			n longOpt: 'new', 		args:1,		'The CSV file containing the latest data'
			s longOpt: 'separator', args:1,		'The column separator to use (defaults to \',\')'
			d longOpt: 'outputdir', args:1,		'The directory into which the comparison results are placed'
			//_ longOpt: 'quoteChar', args:1,		'The character used for quoted elements (defaults to ")'
			//_ longOpt: 'escapeChar',args:1,		'The character used for escaping (defaults to \\)'
			//_ longOpt: 'ignoreLines',  args:1,	'The number of lines (rows) to ignore (defaults to 1)'
			//_ longOpt: 'strictQuotes', args:1,	''

		}
		OptionAccessor options = cli.parse(args)

		if (!options) {
			cli_err("Error: No options provided")
			System.exit(1)
		}

		if (options.h) {
			cli.usage()
			return
		}

		if (options.v) {
			println VERSION
			return
		}

		char separator
		if (options.s) {
			separator = options.s
		} else {
			separator = ','
		}

		String idCol
		if(options.i){
			idCol = options.i
		} else {
			idCol = 'ID'
		}

		if (!options.o || !options.n) {
			cli_err("Error: Missing parameters - try -h")
		}

		if (!check_file_param(options.o)) {
			cli_err("Error: $options.o does not exist")
		}

		if (!check_file_param(options.n)) {
			cli_err("Error: $options.n does not exist")
		}

		if (!check_dir_param(options.d)) {
			cli_err("Error: $options.d does not exist")
		}

		def props = [:]
		props.separator = separator
		props.idColumn = idCol

		CSVProperties csvProps = new CSVProperties(props)

		try {
			CSV newFile = new CSV(fileName:options.n, properties:csvProps)
			CSV oldFile = new CSV(fileName:options.o, properties:csvProps)
			compare(newFile, oldFile, options.d)
		} catch (Exception e){
			cli_err(e.toString())
		}
	}

	private static void compare(CSV newCsv, CSV oldCsv, String out) {
		
		char 
		
		String[] newCols = newCsv.getColumns()
		String[] oldCols = oldCsv.getColumns()

		//Validate the CSVs can be diff'd
		if (!compareList(newCols, oldCols)){
			throw new Exception("Error: The two CSV files appear to have different columns - processing halted")
		} else if (!newCols.contains(newCsv.properties.idColumn) || !oldCols.contains(oldCsv.properties.idColumn)) {
			throw new Exception("Error: The CSVs do not contain the requested ID column - processing halted")
		}

		DB.createTextTable('new', newCols, newCsv.fileName, newCsv.properties.separator)
		DB.createTextTable('old', oldCols, oldCsv.fileName, oldCsv.properties.separator)

		//Grab the ID column to make the SQL easier to read
		String idCol = newCsv.properties.idColumn
		
		DB.createTempTable('session.diff_del', 'SELECT ${idCol} FROM old  WHERE old.${idCol} NOT IN (SELECT ${idCol} FROM new)')
		DB.createTempTable('session.diff_new', 'SELECT n.* FROM new as n WHERE n.${idCol} NOT IN (SELECT old.${idCol} FROM old)')

		//To determine modified records we need to compare every field - yay!
		String mcl = ""
		for (col in newCols) {
			mcl += "n.$col<>o.$col OR "
		}
		def matchColList = mcl[0..-5]
		DB.createTempTable('session.diff_mod', "SELECT n.* FROM new AS n INNER JOIN old AS o ON n.id = o.id  WHERE " + matchColList)

		Writer.writeAll(DB.executeQuery('SELECT * FROM diff_new'), "${out}/new.csv", true, newCsv.properties)
		Writer.writeAll(DB.executeQuery('SELECT * FROM diff_mod'), "${out}/mod.csv", true, newCsv.properties)
		Writer.writeAll(DB.executeQuery('SELECT * FROM diff_del'), "${out}/del.csv", true, newCsv.properties)
	}
}