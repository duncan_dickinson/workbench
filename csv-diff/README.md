# Introduction

# Working with build.gradle

## Quick tests

	./csvdiff.sh -i id -n src/test/resources/new -o src/test/resources/old.csv



## distJar
To create an all-inclusive Jar that's easily distributable:

	`./gradlew distJar`


## Application
The handy Gradle [application plugin](http://www.gradle.org/docs/current/userguide/application_plugin.html) is used

### startScripts


### run
 
	`./gradlew run -Pargs="-v"`
 
	`./gradlew run -Pargs="-h"