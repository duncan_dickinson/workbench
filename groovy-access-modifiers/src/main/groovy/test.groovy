import pkg_one.PrivateClass
import pkg_one.PrivateTraitImpl
import pkg_one.PublicTraitImpl


def priv = new PrivateClass()

assert priv.privateField == 'private'
priv.privateField = 'hacked'
assert priv.privateField == 'hacked'
assert priv.test() == 'private'

def publicTrait = new PublicTraitImpl()
assert publicTrait.pkg_one_PublicTrait__publicField == 'public'

assert publicTrait.test() == 'public'

//def privTrait = new PrivateTraitImpl()
//assert privTrait.PrivateTrait__privateField == 'private'
//assert privTrait.testMethod() == 'private'
