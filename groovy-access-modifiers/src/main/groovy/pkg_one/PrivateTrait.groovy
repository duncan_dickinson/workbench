package pkg_one

trait PrivateTrait {

    private String privateField = 'private'

    private String test() { 'private' }

}
