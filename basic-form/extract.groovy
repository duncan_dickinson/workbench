/*
 * A basic script that extracts form field data from a PDF form
 */

@Grab(group='org.apache.pdfbox', module='pdfbox', version='1.8.8')
import org.apache.pdfbox.pdmodel.PDDocument

//Load the document
def pdf = PDDocument.load(new File('DataGulcher.pdf'), null)

//Get the form data
def form = pdf.getDocumentCatalog().getAcroForm()

def record = [:]

//Process the form
if (form) {
    for (field in form.getFields()) {
        def name = field.getPartialName()
        if (name ==~ /fc-int01-.*/) {
            //Just ignore these as they're control fields
        } else {
            //Small tidy for the keys - make sure we replace the spaces with underscores
            def key = name.replaceAll(' ', '_')
            
            //Use normalize to tidy up multi-line fields
            def val = field.getValue()?.normalize()
            val = val?: '' 
            record[key] = val
        }        
    }
} else {
    println 'The PDF doesn\'t appear to contain a form.'
}

//Close the document
pdf.close()

//Output a YAML record
@Grab(group='org.yaml', module='snakeyaml', version='1.14')
import org.yaml.snakeyaml.Yaml
Yaml yaml = new Yaml()
print yaml.dump(record)

