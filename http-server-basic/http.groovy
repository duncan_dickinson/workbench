/*******************************************************************************
 * Copyright (c) 2014, Duncan Dickinson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 ******************************************************************************/
 
import groovy.util.logging.Log
import groovy.util.CliBuilder
import groovy.text.SimpleTemplateEngine

@Grab(group='org.apache.httpcomponents', module='httpcore', version='4.3.2')
import org.apache.http.ConnectionClosedException
import org.apache.http.HttpConnectionFactory
import org.apache.http.HttpEntity
import org.apache.http.HttpEntityEnclosingRequest
import org.apache.http.HttpException
import org.apache.http.HttpRequest
import org.apache.http.HttpResponse
import org.apache.http.HttpServerConnection
import org.apache.http.HttpStatus
import org.apache.http.entity.ContentType
import org.apache.http.entity.StringEntity
import org.apache.http.impl.DefaultBHttpServerConnection
import org.apache.http.impl.DefaultBHttpServerConnectionFactory
import org.apache.http.protocol.BasicHttpContext
import org.apache.http.protocol.HttpContext
import org.apache.http.protocol.HttpProcessor
import org.apache.http.protocol.HttpProcessorBuilder
import org.apache.http.protocol.HttpRequestHandler
import org.apache.http.protocol.HttpService
import org.apache.http.protocol.ResponseConnControl
import org.apache.http.protocol.ResponseContent
import org.apache.http.protocol.ResponseDate
import org.apache.http.protocol.ResponseServer
import org.apache.http.protocol.UriHttpRequestHandlerMapper
import org.apache.http.util.EntityUtils

/*
 * This script is run using Groovy:
 *      groovy http.groovy [-p <port>]
 */

// Handle command-line parameters
def cli = new CliBuilder(usage:'groovy http.groovy [options]')
cli.with {
    h longOpt: 'help', 'Show usage information'
    p longOpt:'port', args:1, 'Set server port. Default is 9999.'
}

def options = cli.parse(args)

if (options.h) {
    cli.usage()
    return
}

int port = 9999
if (options.p) {
    port = options.port.toInteger()
}

Server.startServer(new HttpHandler(), port)

/**
 * Provides a very basic HTTP server for testing purposes
 *
 * @author Duncan Dickinson
 * @see <a href="https://hc.apache.org/">Apache HTTPComponents project</a>
 * @see <a href="https://hc.apache.org/httpcomponents-core-ga/httpcore/apidocs/">Apache HTTPComponents API</a>
 * @see <a href="http://hc.apache.org/httpcomponents-core-4.3.x/httpcore/examples/org/apache/http/examples/ElementalHttpServer.java">Apache example</a>
 */
class Server {
    /**
     * Prepares and starts the HTTP server
     * 
     * @param handler   An instance of HttpRequestHandler to handle all URIs
     * @param port      The network port on which to run the server. Defaults to 9999
     */
    static void startServer(HttpRequestHandler handler, int port = 9999) {
        // Set up the HTTP protocol processor
        HttpProcessor httpproc = HttpProcessorBuilder.create()
                .add(new ResponseDate())
                .add(new ResponseServer("Test/1.1"))
                .add(new ResponseContent())
                .add(new ResponseConnControl()).build()

        // Set up request handlers
        UriHttpRequestHandlerMapper reqistry = new UriHttpRequestHandlerMapper()
        reqistry.register("*", handler)
        
        // Set up the HTTP service
        HttpService httpService = new HttpService(httpproc, reqistry)

        Thread t = new RequestListenerThread(httpService, port)
        t.setDaemon(false)
        t.start()
    }
}

/**
 * The RequestListenerThread handles incoming requests, starting a new WorkerThread for each request
 *
 * @author Duncan Dickinson
 */
@Log
class RequestListenerThread extends Thread {

    private final HttpConnectionFactory<DefaultBHttpServerConnection> connFactory
    private final ServerSocket serversocket
    private final HttpService httpService

    public RequestListenerThread(HttpService httpService, int port) throws IOException {
        this.connFactory = DefaultBHttpServerConnectionFactory.INSTANCE
        this.serversocket = new ServerSocket(port)
        this.httpService = httpService
    }

    @Override
    public void run() {
        log.info "Listening on port ${this.serversocket.getLocalPort()}"
        while (!Thread.interrupted()) {
            try {
                // Set up HTTP connection
                Socket socket = this.serversocket.accept()
                log.info  "Incoming connection from ${socket.getInetAddress()}"
                HttpServerConnection conn = this.connFactory.createConnection(socket)

                // Start worker thread
                Thread t = new WorkerThread(this.httpService, conn)
                t.setDaemon(true)
                t.start()
            } catch (InterruptedIOException ex) {
                break
            } catch (IOException e) {
                log.severe "I/O error initialising connection thread: ${e.getMessage()}"
                break
            }
        }
    }
}

/**
 * Spawned for each incoming request
 *
 * @author Duncan Dickinson
 */
@Log
class WorkerThread extends Thread {

    private final HttpService httpservice
    private final HttpServerConnection conn

    public WorkerThread(httpservice, conn) {
        super()
        this.httpservice = httpservice
        this.conn = conn
    }

    @Override
    public void run() {
        log.info "New worker thread"
        HttpContext context = new BasicHttpContext(null)
        try {
            while (!Thread.interrupted() && this.conn.isOpen()) {
                this.httpservice.handleRequest(this.conn, context)
            }
        } catch (ConnectionClosedException ex) {
            log.warning "Client closed connection"
        } catch (IOException ex) {
            log.severe "I/O error: ${ex.getMessage()}"
        } catch (HttpException ex) {
            log.severe "Unrecoverable HTTP protocol violation: ${ex.getMessage()}"
        } finally {
            try {
                this.conn.shutdown()
                log.info "Shutting down worker thread"
            } catch (IOException ignore) {}
        }
    }
}

/**
 * Basic handler class for extracting HTTP request information
 *
 * @author Duncan Dickinson
 * @see <a href="http://hc.apache.org/httpcomponents-core-ga/tutorial/html/fundamentals.html#d5e385">HttpRequestHandler in the Apache HC tutorial</a>
 * @see <a href="http://beta.groovy-lang.org/docs/latest/html/gapi/groovy/text/SimpleTemplateEngine.html">Groovy's SimpleTemplateEngine</a>
 */
@Log
class HttpHandler implements HttpRequestHandler  {
    
    /** The template used to display the server request */
    static private final templateBody = '''\
Request info:
---------------------
Request line:
<% 
request.each {k, v -> println "$k: $v"}
%>

Request headers:
<% headers.each {k, v -> 
    println "$k: "
    println "\t$v"
    println ""}
%>

<% if (attachment) { %>
Request attachment: 
<% attachment.each {k, v -> 
    println "$k: "
    println "\t$v"
}
%>
<% } else { %>
No attachment
<% } %>
'''

    static private final engine = new SimpleTemplateEngine()
    static private final template = engine.createTemplate(templateBody)

    @Override
    public void handle(
            final HttpRequest request,
            final HttpResponse response,
            final HttpContext context) throws HttpException, IOException {

        def binding = [:]
        binding['request'] = [
            'Method' : request.getRequestLine().getMethod(),
            'URI' : request.getRequestLine().getUri(),
            'Protocol' : request.getRequestLine().getProtocolVersion().protocol]

        binding['headers'] = [:]
        request.getAllHeaders().each {
            binding['headers'][it.getName()] = it.getValue()
        }

        binding['attachment'] = [:]

        if (request instanceof HttpEntityEnclosingRequest) {
            HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity()
            binding['attachment']['Length'] = entity.getContentLength()
            binding['attachment']['Type'] = entity.getContentType().getValue()
            binding['attachment']['Streaming'] = entity.isStreaming()
            binding['attachment']['Content'] = EntityUtils.toString(entity)
        }

        def htext = template.make(binding).toString()
        log.info "$htext"
        
        response.setStatusCode(HttpStatus.SC_OK)
        StringEntity entity = new StringEntity(
                "$htext",
                ContentType.create("text/plain", "UTF-8"))
        response.setEntity(entity)
    }
}
